package problem1;

public class Textbooks extends Books
{

  private String character;

  public Textbooks(int pages, String message, String character)
  {
    super(pages, message);
    this.character = character;
  }


  public String toString()
  {
    String str = super.toString();
    str += "Character: " + character;
    return str;
  }


}
