package problem1;
//********************************************************************

//File:         Driver.java       
//Author:       Yishu Xin
//Date:         Nov 22, 2017
//Course:       CPS100
//
//Problem Statement:
//Problem 1:
//----------
//Design and implement a set of classes that define various types of reading
//material: books, novels, magazines, technical journals, textbooks, and so on.
//Include data values that describe various attributes of the material, such as 
//the number of pages and the names of the primary characters. Include methods
//that are named appropriately for each class and that print an appropriate 
//message. Create a main driver class to instantiate and exercise several of 
//the classes.

//Inputs:   none
//Outputs:  outputs from methods of several objects from this problem
//
//********************************************************************
public class Driver
{

  public static void main(String[] args)
  {
    // TODO Auto-generated method stub

    Novels novels = new Novels(723, "This is a novel: The Lord of the Rings ",
        "Gandalf the Grey");
    Magazines magazines = new Magazines(60, "", "");
    Textbooks textbooks = new Textbooks(500, "This is a text book",
        "There is no character");


    System.out.println(textbooks);
    magazines.setMessage("This is a magazine");
    magazines.setCharacter("Authors: 12.");
    System.out.println(magazines + magazines.getCharacter());
    System.out.println(novels);


  }

}
