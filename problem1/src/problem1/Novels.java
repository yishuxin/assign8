package problem1;

public class Novels extends Books
{

  private String character = "Gandalf the Grey";

  public Novels(int pages, String message, String character)
  {
    super(pages, message);
    this.character = character;
  }


  public String toString()
  {
    String str = super.toString();
    str += "Character: " + character;
    return str;
  }
}
