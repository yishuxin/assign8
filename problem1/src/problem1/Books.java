package problem1;

public class Books {
	protected int pages = 1500;
	protected String message = "This is a book";
	protected String character = "";

	
	
	public Books(int pages, String message) {
		this.pages = pages;
		this.message = message;
	}
	
	public void setPages(int pages) {
		this.pages = pages;
	}
	public int getPages() {
		return pages;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	
	public void setCharacter(String character) {
		this.character = character;
	}
	public String getCharacter() {
		return character;
	}
	
	public String toString() {
		String str = "";
		str += "Message: " + message + "\n";
		str += "Pages: " + pages + "\n";
		return str;
}

	
	
	
	
	
	
}
